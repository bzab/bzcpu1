# TO DO #

  * DONE: Support for "\_" in labels 
  * Local labels (e.g. labels starting with "\_" are treated as local for certain block (how we can mark the block boundary?)
  * Macroinstructions
 
 instead of 
 `
  MOV #12,R0
  MOV #13,R1
  MOV #AKUKU,R3
  CALL FILL
 `
 
 We should be able to define:
 
 ` MACRO MFILL A,B,C
     MOV A,R0
	 MOV B,R1
	 MOV C,R3
	 CALL FILL
 `
 
 And use it as 
 ` MFILL #12,#13,#AKUKU `
 


    
    
