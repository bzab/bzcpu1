-------------------------------------------------------------------------------
-- Title      : BZCPUv1
-- Project    : bzcpu1
-------------------------------------------------------------------------------
-- File       : bzcpu.vhd
-- Author     : Bartosz M. Zabołotny  <bzab98@gmail.com>
-- Company    : 
-- Created    : 2016-07-27
-- Last update: 2016-08-31
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Simplistic CPU FPGA
-------------------------------------------------------------------------------
-- Copyright (c) 2016 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2016-07-27  1.0      bzab    Created
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;

entity bzcpu is

  port (
    addr_o : out std_logic_vector(15 downto 0);
    data_o : out std_logic_vector(15 downto 0);
    data_i : in  std_logic_vector(15 downto 0);
    mem_wr : out std_logic;
    io_rd  : out std_logic;
    io_wr  : out std_logic;
    err    : out std_logic;
    clk    : in  std_logic;
    rst_p  : in  std_logic);

end entity bzcpu;

architecture rtl of bzcpu is
  -- Working registers (R6 used as a stack pointer)
  type T_REGS is array (0 to 6) of std_logic_vector(15 downto 0);
  signal regs : T_REGS;

  type T_STATE is (ST_RESET, ST_DECODE, ST_HALT,
                   ST_FETCH, ST_FETCH0, ST_FETCH2,
                   ST_MOV_SRC, ST_MOV_DST, ST_MOV_DST_A,
                   ST_IOR_SRC, ST_IOW_DST, ST_IOW_DST_A,
                   ST_ALU_DO1OP, ST_ALU_DO2OP,
                   ST_ALU_SRC1, ST_ALU_SRC1_A, ST_ALU_SRC1_B,
                   ST_ALU_SRC2, ST_ALU_SRC2_A, ST_ALU_SRC2_B,
                   ST_RET1, ST_RET0,
                   ST_CALL, ST_MISS,
                   ST_GET_OP1, ST_GET_OP1_A, ST_GET_OP1_B, ST_GET_OP1_C, ST_GET_OP1_D,
                   ST_JMP, ST_JMP1);
  signal ret_state, state : T_STATE                       := ST_RESET;
  signal PC               : unsigned(15 downto 0)         := (others => '0');
  signal opcode           : std_logic_vector(15 downto 0);
  signal op1, op2, res    : std_logic_vector(15 downto 0);
  signal is_ior           : boolean                       := false;
  -- CPU status register
  signal status           : std_logic_vector(15 downto 0) := (others => '0');
  alias zero_flag         : std_logic is status(15);
  alias sign_flag         : std_logic is status(14);
  alias carry_flag        : std_logic is status(13);

begin  -- architecture rtl

  p1 : process (clk) is
    variable alu_res : unsigned(16 downto 0);
    variable do_jmp  : boolean;
  begin  -- process p1
    if clk'event and clk = '1' then     -- rising clock edge
      mem_wr <= '0';
      io_rd  <= '0';
      io_wr  <= '0';
      if rst_p = '1' then               -- synchronous reset (active high)
        state  <= ST_RESET;
        err    <= '0';
        addr_o <= (others => '0');
        data_o <= (others => '0');
      else
        case state is
          when ST_RESET =>
            opcode <= data_i;
            addr_o <= std_logic_vector(PC);
            state  <= ST_FETCH;
          when ST_FETCH0 =>
            addr_o <= std_logic_vector(PC);
            state  <= ST_FETCH;
          when ST_FETCH =>
            state <= ST_FETCH2;
          when ST_FETCH2 =>
            opcode <= data_i;
            is_ior <= false;
            state  <= ST_DECODE;
          when ST_DECODE =>
            -- We decode the instruction read from data_i
            if std_match(opcode, "1"&"-----"&"----"&"---"&"---") then
              -- ALU command
              if std_match(opcode(14 downto 11), "1111") then
                -- Service the 1-op case
                ret_state <= ST_ALU_DO1OP;
                state     <= ST_ALU_SRC2;
              else
                -- Service the 2-op case
                -- Load both operands first, then check and perform the operation...
                ret_state <= ST_ALU_SRC2;
                state     <= ST_ALU_SRC1;
              end if;
            elsif std_match(opcode, "0"&"00001"&"----"&"---"&"---") then
              -- MOV command
              ret_state <= ST_MOV_DST;
              state     <= ST_MOV_SRC;
            elsif std_match(opcode, "0"&"00010"&"----"&"---"&"---") then
              -- IOR command
              ret_state <= ST_MOV_DST;
              state     <= ST_IOR_SRC;
              is_ior    <= true;
            elsif std_match(opcode, "0"&"00011"&"----"&"---"&"---") then
              -- IOW command
              ret_state <= ST_IOW_DST;
              state     <= ST_MOV_SRC;
            elsif std_match(opcode, "0"&"11111"&"0000"&"---"&"---") then
              -- JMP command
              do_jmp := false;
              case opcode(5 downto 3) is
                when "000" =>           -- JMP
                  do_jmp := true;
                when "001" =>           -- JZ (if zero)
                  if zero_flag = '1' then
                    do_jmp := true;
                  end if;
                when "010" =>           -- JNZ (if not zero)
                  if zero_flag = '0' then
                    do_jmp := true;
                  end if;
                when others =>
                  err   <= '1';
                  state <= ST_HALT;
              end case;
              state <= ST_GET_OP1;
              if do_jmp then
                ret_state <= ST_JMP;
              else
                ret_state <= ST_MISS;
              end if;
            elsif std_match(opcode, "0"&"11111"&"0001"&"---"&"---") then
              -- CAL command
              do_jmp := false;
              case opcode(5 downto 3) is
                when "000" =>           -- CALL
                  do_jmp := true;
                when "001" =>           -- CZ (if zero)
                  if zero_flag = '1' then
                    do_jmp := true;
                  end if;
                when "010" =>           -- CNZ (if not zero)
                  if zero_flag = '0' then
                    do_jmp := true;
                  end if;
                when others =>
                  err   <= '1';
                  state <= ST_HALT;
              end case;
              state <= ST_GET_OP1;
              if do_jmp then
                ret_state <= ST_CALL;
              else
                ret_state <= ST_MISS;
              end if;
              null;
            elsif std_match(opcode, "0"&"11111"&"0010"&"111"&"---") then
              -- RET command
              do_jmp := false;
              case opcode(2 downto 0) is
                when "000" =>           -- RET
                  do_jmp := true;
                when "001" =>           -- RZ (if zero)
                  if zero_flag = '1' then
                    do_jmp := true;
                  end if;
                when "010" =>           -- RNZ (if not zero)
                  if zero_flag = '0' then
                    do_jmp := true;
                  end if;
                when others =>
                  err   <= '1';
                  state <= ST_HALT;
              end case;
              if do_jmp then
                addr_o  <= std_logic_vector(unsigned(regs(6))+1);
                regs(6) <= std_logic_vector(unsigned(regs(6))+1);
                state   <= ST_RET0;
              else
                PC     <= PC+1;
                addr_o <= std_logic_vector(PC+1);
                state  <= ST_FETCH;
              end if;
            elsif std_match(opcode, "0"&"00000"&"0000"&"000"&"000") then
              -- NOP command
              PC     <= PC+1;
              addr_o <= std_logic_vector(PC+1);
              state  <= ST_FETCH;
            end if;
          when ST_RET0 =>
            -- Memory is addressed, we wait one clock pulse more to receive the
            -- data
            state <= ST_RET1;
          when ST_RET1 =>
            -- Memory is addressed, we wait one clock pulse more to receive the
            -- data
            PC     <= unsigned(data_i);
            addr_o <= data_i;
            state  <= ST_FETCH;
          when ST_JMP =>
            PC     <= unsigned(op1);
            addr_o <= std_logic_vector(unsigned(op1));
            state  <= ST_FETCH;
          when ST_CALL =>
            data_o  <= std_logic_vector(PC+1);
            addr_o  <= regs(6);
            regs(6) <= std_logic_vector(unsigned(regs(6))-1);
            mem_wr  <= '1';
            PC      <= unsigned(op1);
            state   <= ST_FETCH0;
          when ST_MISS =>
            -- Increase the address after skipped JMP or CALL
            PC     <= PC+1;
            addr_o <= std_logic_vector(PC+1);
            state  <= ST_FETCH;
          -- States related to fetching the destination address for JMP and CAL
          when ST_GET_OP1 =>
            case to_integer(unsigned(opcode(2 downto 0))) is
              when 0 to 5 =>
                op1   <= regs(to_integer(unsigned(opcode(2 downto 0))));
                state <= ST_JMP;
              when 6 =>
                addr_o <= std_logic_vector(PC+1);
                PC     <= PC+1;
                state  <= ST_GET_OP1_A;
              when 7 =>
                PC     <= PC+1;
                addr_o <= std_logic_vector(PC+1);
                state  <= ST_GET_OP1_C;
              when others => null;
            end case;
          when ST_GET_OP1_A =>
            state <= ST_GET_OP1_B;
          when ST_GET_OP1_B =>
            op1   <= data_i;
            state <= ret_state;
          when ST_GET_OP1_C =>
            state <= ST_GET_OP1_D;
          when ST_GET_OP1_D =>
            addr_o <= data_i;
            if is_ior then
              io_rd <= '1';
            end if;
            state <= ST_GET_OP1_A;
          -- states related to fetching the SRC address for MOV
          when ST_MOV_SRC =>
            case to_integer(unsigned(opcode(9 downto 5))) is
              when 0 to 6 =>
                -- Registers and immediate
                op1   <= regs(to_integer(unsigned(opcode(9 downto 5))));
                state <= ret_state;
              when 7 =>
                -- Immediate
                PC     <= PC+1;
                addr_o <= std_logic_vector(PC+1);
                state  <= ST_GET_OP1_A;
              when 8 to 14 =>
                -- Indirect register addressing
                addr_o <= regs(to_integer(unsigned(opcode(9 downto 5)))-8);
                state  <= ST_GET_OP1_A;
              when 15 =>
                -- Indirect through memory pointer
                PC     <= PC+1;
                addr_o <= std_logic_vector(PC+1);
                state  <= ST_GET_OP1_C;
              when 16 to 22 =>
                -- Indirect register with preincrementation
                regs(to_integer(unsigned(opcode(9 downto 5)))-16) <= std_logic_vector(unsigned(regs(to_integer(unsigned(opcode(9 downto 5)))-16)) + 1);
                addr_o                                            <= std_logic_vector(unsigned(regs(to_integer(unsigned(opcode(9 downto 5)))-16))+1);
                state                                             <= ST_GET_OP1_A;
              when 24 to 30 =>
                -- Indirect register with postdecrementation
                regs(to_integer(unsigned(opcode(9 downto 5)))-24) <= std_logic_vector(unsigned(regs(to_integer(unsigned(opcode(9 downto 5)))-24)) - 1);
                addr_o                                            <= std_logic_vector(unsigned(regs(to_integer(unsigned(opcode(9 downto 5)))-24)));
                state                                             <= ST_GET_OP1_A;
              when others =>
                state <= ST_HALT;
            end case;
          when ST_MOV_DST =>
            case to_integer(unsigned(opcode(4 downto 0))) is
              when 0 to 6 =>
                regs(to_integer(unsigned(opcode(4 downto 0)))) <= op1;
                PC                                             <= PC+1;
                addr_o                                         <= std_logic_vector(PC+1);
                -- We may send PC+1 to addr_o so we can go to ST_FETCH
                state                                          <= ST_FETCH;
              when 7 =>
                data_o    <= op1;
                addr_o    <= std_logic_vector(PC+1);
                PC        <= PC+1;
                ret_state <= ST_MOV_DST_A;
                state     <= ST_GET_OP1_A;
              when 8 to 14 =>
                -- Indirect register addressing
                data_o <= op1;
                addr_o <= regs(to_integer(unsigned(opcode(4 downto 0)))-8);
                mem_wr <= '1';
                PC     <= PC+1;
                state  <= ST_FETCH0;
              when 15 =>
                -- Indirect through memory pointer
                PC        <= PC+1;
                data_o    <= op1;
                addr_o    <= std_logic_vector(PC+1);
                state     <= ST_GET_OP1_C;
                ret_state <= ST_MOV_DST_A;
              when 16 to 22 =>
                -- Indirect register with preincrementation
                data_o                                            <= op1;
                regs(to_integer(unsigned(opcode(4 downto 0)))-16) <= std_logic_vector(unsigned(regs(to_integer(unsigned(opcode(4 downto 0)))-16)) + 1);
                addr_o                                            <= std_logic_vector(unsigned(regs(to_integer(unsigned(opcode(4 downto 0)))-16))+1);
                PC                                                <= PC+1;
                mem_wr                                            <= '1';
                state                                             <= ST_FETCH0;
              when 24 to 30 =>
                -- Indirect register with postdecrementation
                data_o                                            <= op1;
                regs(to_integer(unsigned(opcode(4 downto 0)))-24) <= std_logic_vector(unsigned(regs(to_integer(unsigned(opcode(4 downto 0)))-24)) - 1);
                addr_o                                            <= std_logic_vector(unsigned(regs(to_integer(unsigned(opcode(4 downto 0)))-24)));
                mem_wr                                            <= '1';
                PC                                                <= PC+1;
                state                                             <= ST_FETCH0;
              when others =>
                state <= ST_HALT;
            end case;
          when ST_MOV_DST_A =>
            addr_o <= op1;
            mem_wr <= '1';
            PC     <= PC+1;
            -- We can't send PC+1 to addr_o yet (it is used for memory writing), so we have to go to ST_FETCH0
            state  <= ST_FETCH0;
          when ST_IOR_SRC =>
            case to_integer(unsigned(opcode(9 downto 5))) is
              when 7 =>
                -- Immediate
                PC     <= PC+1;
                addr_o <= std_logic_vector(PC+1);
                state  <= ST_GET_OP1_C;
              when 8 to 14 =>
                --  Indirect Register
                addr_o <= regs(to_integer(unsigned(opcode(9 downto 5)))-8);
                io_rd  <= '1';
                state  <= ST_GET_OP1_A;
              when 15 =>
                -- Indirect Addres
                PC     <= PC+1;
                addr_o <= std_logic_vector(PC+1);
                state  <= ST_GET_OP1_C;
              when 16 to 22 =>
                -- Indirect Register with PreInc
                regs(to_integer(unsigned(opcode(9 downto 5)))-16) <= std_logic_vector(unsigned(regs(to_integer(unsigned(opcode(9 downto 5)))-16)) + 1);
                addr_o                                            <= std_logic_vector(unsigned(regs(to_integer(unsigned(opcode(9 downto 5)))-16))+1);
                io_rd                                             <= '1';
                state                                             <= ST_GET_OP1_A;
              when 24 to 30 =>
                -- Indirect Register with PostDec
                regs(to_integer(unsigned(opcode(9 downto 5)))-24) <= std_logic_vector(unsigned(regs(to_integer(unsigned(opcode(9 downto 5)))-24)) - 1);
                addr_o                                            <= std_logic_vector(unsigned(regs(to_integer(unsigned(opcode(9 downto 5)))-24)));
                io_rd                                             <= '1';
                state                                             <= ST_GET_OP1_A;
              when others =>
                state <= ST_HALT;
            end case;
          when ST_IOW_DST =>
            case to_integer(unsigned(opcode(4 downto 0))) is
              when 7 =>
                -- Immediate
                data_o    <= op1;
                addr_o    <= std_logic_vector(PC+1);
                PC        <= PC+1;
                ret_state <= ST_IOW_DST_A;
                state     <= ST_GET_OP1_A;
              when 8 to 14 =>
                -- Indirect Register
                data_o <= op1;
                addr_o <= regs(to_integer(unsigned(opcode(4 downto 0)))-8);
                io_wr  <= '1';
                PC     <= PC+1;
                state  <= ST_FETCH0;
              when 15 =>
                -- Indirect
                PC        <= PC+1;
                data_o    <= op1;
                addr_o    <= std_logic_vector(PC+1);
                state     <= ST_GET_OP1_C;
                ret_state <= ST_IOW_DST_A;
              when 16 to 22 =>
                -- Indirect Register with PreInc
                data_o                                            <= op1;
                regs(to_integer(unsigned(opcode(4 downto 0)))-16) <= std_logic_vector(unsigned(regs(to_integer(unsigned(opcode(4 downto 0)))-16)) + 1);
                addr_o                                            <= std_logic_vector(unsigned(regs(to_integer(unsigned(opcode(4 downto 0)))-16))+1);
                PC                                                <= PC+1;
                io_wr                                             <= '1';
                state                                             <= ST_FETCH0;
              when 24 to 30 =>
                -- Indirect register with PostDec
                data_o                                            <= op1;
                regs(to_integer(unsigned(opcode(4 downto 0)))-24) <= std_logic_vector(unsigned(regs(to_integer(unsigned(opcode(4 downto 0)))-24)) - 1);
                addr_o                                            <= std_logic_vector(unsigned(regs(to_integer(unsigned(opcode(4 downto 0)))-24)));
                io_wr                                             <= '1';
                PC                                                <= PC+1;
                state                                             <= ST_FETCH0;
              when others =>
                state <= ST_HALT;
            end case;
          when ST_IOW_DST_A =>
            addr_o <= op1;
            io_wr  <= '1';
            PC     <= PC+1;
            -- We can't send PC+1 to addr_o yet (it is used for memory writing), so we have to go to ST_FETCH0
            state  <= ST_FETCH0;
          -- Error handling
          when ST_HALT =>
            err   <= '1';
            state <= ST_HALT;
          when ST_ALU_SRC1 =>
            case to_integer(unsigned(opcode(10 downto 8))) is
              when 0 to 6 =>
                --Registers
                op1   <= regs(to_integer(unsigned(opcode(10 downto 8))));
                state <= ret_state;
              when 7 =>
                --Immediate
                addr_o <= std_logic_vector(PC+1);
                PC     <= PC+1;
                state  <= ST_ALU_SRC1_A;
              when others => null;
            end case;
          when ST_ALU_SRC1_A =>
            state <= ST_ALU_SRC1_B;
          when ST_ALU_SRC1_B =>
            op1   <= data_i;
            state <= ret_state;
          when ST_ALU_SRC2 =>
            ret_state <= ST_ALU_DO2OP;
            case to_integer(unsigned(opcode(7 downto 5))) is
              when 0 to 6 =>
                --Registers
                op1   <= regs(to_integer(unsigned(opcode(7 downto 5))));
                state <= ret_state;
              when 7 =>
                --Immediate
                addr_o <= std_logic_vector(PC+1);
                PC     <= PC+1;
                state  <= ST_ALU_SRC2_A;
              when others => null;
            end case;
          when ST_ALU_SRC2_A =>
            state <= ST_ALU_SRC2_B;
          when ST_ALU_SRC2_B =>
            op2   <= data_i;
            state <= ret_state;
          when ST_ALU_DO2OP =>
            case opcode(14 downto 11) is
              when x"0" =>
                -- ST_ALU_AND 
                alu_res   := unsigned('0' & op1) and unsigned('0' & op2);
                if alu_res = x"0000" then
                  zero_flag <= '1';
                else
                  zero_flag <= '0';
                end if;
                op1        <= std_logic_vector(alu_res(15 downto 0));
                state      <= ST_MOV_DST;
              when x"3" =>
                -- ST_ALU_ADD 
                alu_res   := unsigned('0' & op1) + unsigned('0' & op2);
                sign_flag <= alu_res(15);
                if alu_res = x"0000" then
                  zero_flag <= '1';
                else
                  zero_flag <= '0';
                end if;
                carry_flag <= alu_res(16);
                op1        <= std_logic_vector(alu_res(15 downto 0));
                state      <= ST_MOV_DST;
              when x"5" =>
                -- ST_ALU_SUB
                alu_res   := unsigned('0' & op1) - unsigned('0' & op2);
                sign_flag <= alu_res(15);
                if alu_res = x"0000" then
                  zero_flag <= '1';
                else
                  zero_flag <= '0';
                end if;
                carry_flag <= alu_res(16);
                op1        <= std_logic_vector(alu_res(15 downto 0));
                state      <= ST_MOV_DST;
              when x"7" =>
                -- ST_ALU_CMP
                alu_res   := unsigned('0' & op1) - unsigned('0' & op2);
                sign_flag <= alu_res(15);
                if alu_res = x"0000" then
                  zero_flag <= '1';
                else
                  zero_flag <= '0';
                end if;
                carry_flag <= alu_res(16);
                --op1        <= std_logic_vector(alu_res(15 downto 0));
                addr_o     <= std_logic_vector(PC+1);
                PC         <= PC+1;
                state      <= ST_FETCH;
              when others => null;
            end case;
          when others => null;
        end case;
      end if;
    end if;
  end process p1;


end architecture rtl;

