-------------------------------------------------------------------------------
-- Title      : Testbench for design "bzcpu"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : bzcpu_tb.vhd
-- Author     : Wojciech M. Zabołotny  <wzab@awzsrv.nasz.dom>
-- Company    : 
-- Created    : 2016-07-27
-- Last update: 2016-08-15
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2016 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2016-07-27  1.0      wzab    Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;

-------------------------------------------------------------------------------

entity bzcpu_tb is

end entity bzcpu_tb;

-------------------------------------------------------------------------------

architecture test of bzcpu_tb is

  constant IO_UART_DATA   : integer := 16#8001#;
  constant IO_UART_STATUS : integer := 16#8002#;

  -- component ports
  signal addr_o     : std_logic_vector(15 downto 0);
  signal data_o     : std_logic_vector(15 downto 0);
  signal data_i     : std_logic_vector(15 downto 0);
  signal mem_data_i : std_logic_vector(15 downto 0);
  signal mem_wr     : std_logic;
  signal io_rd      : std_logic;
  signal io_wr      : std_logic;
  signal err        : std_logic;
  signal rst_p      : std_logic := '0';

  type T_RAM_PROG is array(0 to 65535) of std_logic_vector(15 downto 0);
  -- Initialization of the memory

  procedure read_hex_stlv (
    variable fline : inout line;
    constant nbits :       integer;
    variable res   : out   std_logic_vector) is

    variable tmp          : std_logic_vector((nbits+3) downto 0) := (others => '0');
    variable c            : character;
    variable npos, nchars : integer;
  begin  -- readhex
    nchars := (nbits+3)/4;              -- number of hex chars to read
    for i in nchars-1 downto 0 loop
      npos := i*4+3;
      read (fline, c);
      case c is
        when '0' =>
          tmp(npos downto npos-3) := "0000";
        when '1' =>
          tmp(npos downto npos-3) := "0001";
        when '2' =>
          tmp(npos downto npos-3) := "0010";
        when '3' =>
          tmp(npos downto npos-3) := "0011";
        when '4' =>
          tmp(npos downto npos-3) := "0100";
        when '5' =>
          tmp(npos downto npos-3) := "0101";
        when '6' =>
          tmp(npos downto npos-3) := "0110";
        when '7' =>
          tmp(npos downto npos-3) := "0111";
        when '8' =>
          tmp(npos downto npos-3) := "1000";
        when '9' =>
          tmp(npos downto npos-3) := "1001";
        when 'a' =>
          tmp(npos downto npos-3) := "1010";
        when 'A' =>
          tmp(npos downto npos-3) := "1010";
        when 'b' =>
          tmp(npos downto npos-3) := "1011";
        when 'B' =>
          tmp(npos downto npos-3) := "1011";
        when 'c' =>
          tmp(npos downto npos-3) := "1100";
        when 'C' =>
          tmp(npos downto npos-3) := "1100";
        when 'd' =>
          tmp(npos downto npos-3) := "1101";
        when 'D' =>
          tmp(npos downto npos-3) := "1101";
        when 'e' =>
          tmp(npos downto npos-3) := "1110";
        when 'E' =>
          tmp(npos downto npos-3) := "1110";
        when 'f' =>
          tmp(npos downto npos-3) := "1111";
        when 'F' =>
          tmp(npos downto npos-3) := "1111";
        when others =>
          assert(false)
            report "Error: wrong separator in the write command" severity error;
      end case;
    end loop;  -- i
    res := tmp((nbits-1) downto 0);
  end read_hex_stlv;

  procedure read_ram (
    variable vram : inout T_RAM_PROG) is
    file ramini      : text;
    variable line_in : line;
    variable i, vali : integer := 0;
    variable valv    : std_logic_vector(15 downto 0);
  begin
    file_open(ramini, "ram.hex", read_mode);
    while true loop
      if endfile(ramini) then
        exit;
      end if;
      readline(ramini, line_in);
      read_hex_stlv(line_in, 16, valv);
      --vali := to_integer(unsigned(valv));
      --report integer'image(vali) severity note;
      vram(i) := valv;
      i       := i+1;
    end loop;
    report "finished RAM initialization" severity note;
    file_close(ramini);
  end procedure;

  -- Program and data memory
  shared variable ram : T_RAM_PROG := (others => (others => '0'));

  -- clock
  signal Clk                              : std_logic := '1';
  signal i_addr                           : integer;
  signal uart_data_o                      : std_logic_vector(7 downto 0);
  signal uart_dav, uart_empty, uart_ready : std_logic;
  signal uart_rd, uart_wr : std_logic;

begin  -- architecture test

  -- component instantiation
  DUT : entity work.bzcpu
    port map (
      addr_o => addr_o,
      data_o => data_o,
      data_i => data_i,
      mem_wr => mem_wr,
      io_rd  => io_rd,
      io_wr  => io_wr,
      err    => err,
      clk    => clk,
      rst_p  => rst_p);

  i_addr <= to_integer(unsigned(addr_o));

  ghdl_uart_1 : entity work.ghdl_uart
    port map (
      data_out => uart_data_o,
      data_in  => data_o(7 downto 0),
      dav      => uart_dav,
      ready    => uart_ready,
      empty    => uart_empty,
      rd       => uart_rd,
      wr       => uart_wr);

  uart_wr <= '1' when (i_addr = IO_UART_DATA) and (io_wr = '1') else '0';
  uart_rd <= '1' when (i_addr = IO_UART_DATA) and (io_rd = '1') else '0';

  process (i_addr, mem_data_i, uart_data_o, uart_dav, uart_empty, uart_ready) is
  begin  -- process
    case i_addr is
      when IO_UART_DATA =>
        data_i           <= (others => '0');
        data_i(7 downto 0) <= uart_data_o;
      when IO_UART_STATUS =>
        data_i    <= (others => '0');
        data_i(0) <= uart_dav;
        data_i(1) <= uart_ready;
        data_i(2) <= uart_empty;
      when others =>
        data_i <= mem_data_i;
    end case;
  end process;

  -- clock generation
  Clk <= not Clk after 10 ns;

  p_ram : process (clk) is
  begin  -- process
    if clk'event and clk = '1' then     -- rising clock edge
      if mem_wr = '1' then
        ram(to_integer(unsigned(addr_o))) := data_o;
        report "MEM WR addr=" & integer'image(to_integer(unsigned(addr_o))) & " data=" & integer'image(to_integer(unsigned(data_o))) severity note;
      end if;
      mem_data_i <= ram(to_integer(unsigned(addr_o)));
    end if;
  end process;


  -- waveform generation
  WaveGen_Proc : process
  begin
    -- insert signal assignments here
    read_ram(ram);
    wait until Clk = '1';
    wait for 25 ns;
    rst_p <= '0';
    wait;
  end process WaveGen_Proc;



end architecture test;

